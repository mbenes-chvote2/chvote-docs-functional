# Data set content

## Control components Key

Contains the control component public keys needed to verify the signature of the printer archive.

## Electoral Authority Key

Contains the electoral authority key to decipher the tally archive in order to get vote counts.

The electoral authority key is split into 3 parts, with 2 parts necessary to reconstitute
the key. 

Key parts password are `simulation password!`.

## Printer Authority Key

Contains the key needed to decipher the printer archive and generate the eCH-0228 files.

Key password is `simulation password!`.

## Scenario1_VP_QS_FED

Contains files needed for a sample votation with one simple question on a federal subject

## Scenario2_VP_QC_FEDCANCOM

Contains the files needed for a sample votation with some questions on a federal, cantonal,  and communal subject:

* one simple question on a federal subject (yes, no, blank answers)
* one complex question on a cantonal subject (yes, no, blank, IN, CP answers)
* one simple question on a communal subject (yes, no, blank answers)

## Scenario3_VP_QS_RepLibres

Contains the files needed for a sample votation with :

* one simple question on a federal subject (Variante1, Variante2, blank answers) 